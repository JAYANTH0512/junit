package com.innominds.Assignment.Questions;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

class Question6Test {

	Question6 q6 = new Question6();
	
	@Test
	public void test() {
		
		LocalDate start_date1 = LocalDate.of(2018, 01, 10);
		 LocalDate end_date1   = LocalDate.of(2020, 06, 10);
		 assertEquals("2,5,0",q6.findDifference(start_date1,end_date1));
		
	}




}
