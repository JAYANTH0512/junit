package com.innominds.Assignment.Questions;

import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.junit.jupiter.api.Test;

class Question5Test {

	@Test
	void testdate() {
		SimpleDateFormat dateformate = new SimpleDateFormat("dd/MM/yyyy  z");
		Date date1 = new Date();
		dateformate.setTimeZone(TimeZone.getTimeZone("IST"));
		dateformate.format(date1);
		assertEquals(dateformate.format(date1),Question5.dateandtime());
		
	}
	@Test
	void testdate2() {
		SimpleDateFormat dateformate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss  z");
		Date date1 = new Date();
		Date date2 = new Date(date1.getTime() + 10 * 60 * 60 * 1000);
		dateformate.setTimeZone(TimeZone.getTimeZone("IST"));
		dateformate.format(date2);
		assertEquals(dateformate.format(date2), Question5.dateandtime2());
    }
}