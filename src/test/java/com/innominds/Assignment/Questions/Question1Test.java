package com.innominds.Assignment.Questions;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Question1Test {
	
	Question1 q1 = new Question1();

	@Test
	void testUpper() {
		char ch ='A';
		assertEquals("UpperCase",q1.Cases(ch));
	}
	
	
	@Test
	void testLower() {
		char ch = 'a';
		assertEquals("LowerCase",q1.Cases(ch));
	}
	
	@Test
	void testDigit() {	
		char ch ='2';
		assertEquals("Number of Digits",q1.Cases(ch));
		
	}
	
	@Test	
	void testOthers() {
		
	char ch ='@';
	assertEquals("Others",q1.Cases(ch));
	
	}
}