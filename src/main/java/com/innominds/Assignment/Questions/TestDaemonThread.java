package com.innominds.Assignment.Questions;

//Write unit tests for a Program illustrating Daemon Threads


public class TestDaemonThread extends Thread {
	public void run() {
		
		TestDaemonThread t1=new TestDaemonThread();
		t1.setDaemon(true);
		t1.start();
		
		if (Thread.currentThread().isDaemon()) {
			System.out.println("demon thread work");
			
		} else {
			System.out.println("User thread work");
		}
	}

}
