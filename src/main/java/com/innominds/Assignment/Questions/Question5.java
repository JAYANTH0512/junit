package com.innominds.Assignment.Questions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * 4.Write unit tests for Print the current date with time zone,
 *  Add 10Hours to the existing date and print
 * @author jpalepally1
 *
 */

public class Question5 {
	

		public static String  dateandtime() {
			
			SimpleDateFormat dateformate = new SimpleDateFormat("dd/MM/yyyy  z");
			Date date = new Date();
			dateformate.setTimeZone(TimeZone.getTimeZone("IST"));
			return (dateformate.format(date));
							
		}
		
		public static String dateandtime2() {
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss  z");
			Date date1 = new Date();
			Date date2 = new Date(date1.getTime() + 10 * 60 * 60 * 1000);
			format.setTimeZone(TimeZone.getTimeZone("IST"));
		    return (format.format(date2));
		}
		public static void main(String[] args) {
			Question5 q = new Question5();
			System.out.println(q.dateandtime());
			System.out.println(q.dateandtime2());
		}
		
	}
	
	
	


