package com.innominds.Assignment.Questions;


/**
 * This is the to write the test cases to check the input character is upper,
 *lower,number of digits and other Characters
 * @author jpalepally1
 *
 */
public class Question1 {
	
	public String Cases(char ch) {
		
		if (Character.isUpperCase(ch)) {
			return "UpperCase";
	   } else if(Character.isLowerCase(ch)) {
			return "LowerCase";
		}else if(Character.isDigit(ch)){
			return "Number of Digits";
		}else{
			return "Others";
		}
	}

}
