package com.innominds.Assignment.Questions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Question4 {
	
	
	public class demo {
		String URL = "jdbc:mysql://localhost:3306/jayanth"; // This is the Connection URL
		String USER = "root"; // This is MYSQL User name
		String PASSWORD = "jayanth"; // This is the password

		
		public void database() {

			try {
				
				String Query = ("select * from employee"); // This is the Query need to be Executed

				Class.forName("com.mysql.cj.jdbc.Driver"); // Registering the driver
				Connection conn = DriverManager.getConnection(URL, USER, PASSWORD); // Creating the Connection
				Statement st = conn.createStatement(); // Creating the statement
				ResultSet rs = st.executeQuery(Query); // Executing the Query

				while (rs.next()) {

					int id = rs.getInt("id");
					String name = rs.getString("name");
					String designation = rs.getString("designation");
					System.out.println(id + "\t\t" + name + "\t\t" + designation);
				}
				st.close(); // Closing the statement
				conn.close(); // Closing the connection

			} catch (Exception e) {
				System.out.println(e);
			}

		}

		public void countTables() {
	        Statement stmt = null;
	        ResultSet resultset = null;
	        try {
	            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
	            stmt = conn.createStatement();
	              String query = "select count(*) as  totalnumberoftables from information_schema.tables where table_schema='jayanth'";
	              
	              ResultSet rs = stmt.executeQuery(query);
	              rs.next();
	              int count = rs.getInt(1);
	              System.out.println("Number of records in the jayanth table: "+count);
	            
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
		

	}

}
